﻿using Contracts;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class ProductRepository: RepositoryBase<Product>,IProductRepository
    {
        public ProductRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Product> GetAllProduct()
        {
           
                return FindAll().OrderBy(pr => pr.ProductName).ToList();
           
        }

        public Product GetProductById(int productId)
        {
            return FindByCondition(pr => pr.ProductId.Equals(productId)).FirstOrDefault();
        }
    }
}
