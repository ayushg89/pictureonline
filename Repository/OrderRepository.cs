﻿using Contracts;
using Entities;
using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {

        public OrderRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        private decimal RequiredBinWidth;

        public OrderExtended GetOrderById(int orderId)
        {
            var productDetails = (from po in RepositoryContext.ProductOrders
                                  join pr in RepositoryContext.Products
                                 on po.ProductId equals pr.ProductId
                                  where po.OrderId == orderId
                                  select new
                                  {
                                      ProductName1 = pr.ProductName,
                                      ProductQuantity = po.Quantity
                                  }).ToList();
            if(!RepositoryContext.Orders.Where(o => o.OrderId == orderId).
                Select(o => o.OrderMinimumWidth).Any())
            {
                return new OrderExtended();
            }
            var minimumWidth = RepositoryContext.Orders.Where(o => o.OrderId == orderId).
                Select(o => o.OrderMinimumWidth).Single();
            List<ProductQuantity> lst = new List<ProductQuantity>();
            foreach (var item in productDetails)
            {
                ProductQuantity pqr = new ProductQuantity
                {
                    ProductName = item.ProductName1.ToString(),
                    Quantity = item.ProductQuantity
                };
                lst.Add(pqr);
            };
            OrderExtended order = new OrderExtended
            {
                OrderId = orderId,
                OrderMinimumWidth = minimumWidth,
                Products = lst
            };


            return order;
        }

        public decimal CreatOrder(List<NewOrderRequestObj> orderDetails)
        {
            CalculateMinimumWidthForOrder(orderDetails);                // Calculate minimum width required for the order
            if(RequiredBinWidth!=0)
            {
                int newOrderId = InsertOrder();                             //Save data in order table
                InsertProductOrder(orderDetails, newOrderId);               //Save data in productorder table
            }
            return RequiredBinWidth;                                             //Return RequiredBinWidth 
        }

        private void CalculateMinimumWidthForOrder(List<NewOrderRequestObj> orderDetails)
        {
            decimal productBasewidth=0;
            foreach (var item in orderDetails)
            {
                if (item.ProductQuantity == 0)
                {
                    break;
                }
                if(!RepositoryContext.Products.Where(pr => pr.ProductId == item.ProductId).Select(pr => pr.ProductWidth).Any())
                {
                    break;
                }
                productBasewidth = RepositoryContext.Products.Where(pr => pr.ProductId == item.ProductId).Select(pr => pr.ProductWidth).Single();          
                if ((int)ProductType.mug == item.ProductId)
                {
                    int maximumnumberofMugStack = 4;
                    if (item.ProductQuantity > maximumnumberofMugStack)
                    {
                        int div = item.ProductQuantity / maximumnumberofMugStack;
                        int mod = item.ProductQuantity % maximumnumberofMugStack;
                        if (mod > 0)
                        {
                            div++;
                        }
                        RequiredBinWidth += productBasewidth * div;
                    }
                    else
                    {
                        RequiredBinWidth += productBasewidth;                      
                    }                   
                }
                else
                { 
                    RequiredBinWidth += productBasewidth * item.ProductQuantity;
                }

            }
        }

        private int InsertOrder()
        {
            Order order = new Order();
            int maxorderId = RepositoryContext.Orders.Max(o => o.OrderId);
            int neworderId = maxorderId + 1;
            order.OrderId = neworderId;
            order.OrderMinimumWidth = RequiredBinWidth;
            AddOrder(order);
            return neworderId;
        }

        private void InsertProductOrder(List<NewOrderRequestObj> orderDetails, int newOrderId)
        {
            List<ProductOrder> lst = new List<ProductOrder>();
            foreach (var item in orderDetails)
            {
                ProductOrder productOrder = new ProductOrder
                {
                    OrderId = newOrderId,
                    ProductId = item.ProductId,
                    Quantity = item.ProductQuantity
                };
                lst.Add(productOrder);

            }
            RepositoryContext.ProductOrders.AddRange(lst);
            RepositoryContext.SaveChanges();                        //Insert all the changes (For our case : Order and ProductOrder)
        }

        public void AddOrder(Order order)
        {
            Create(order); //Add Order detils in order entity
        }       
    }
}








