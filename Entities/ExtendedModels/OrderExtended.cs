﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ExtendedModels
{
    public class OrderExtended
    {

        public int OrderId { get; set; }

        public decimal OrderMinimumWidth { get; set; }

        public List<ProductQuantity> Products { get; set; }

        public OrderExtended()
        {
        }
    }

    public class ProductQuantity
    {
        
        public string ProductName { get; set; }
        public int Quantity { get; set; }

       
    }
}
