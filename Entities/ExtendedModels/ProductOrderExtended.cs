﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ExtendedModels
{
    public class ProductOrderExtended
    {
        public string productname { get; set; }

        public int productquantity { get; set; }
    }
}
