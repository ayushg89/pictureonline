﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("productorder")]
    public class ProductOrder
    {
        [Key]
        public int id { get; set; }

        [Required]
        public int OrderId { get; set; }

        [Required]
        public int  ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}
