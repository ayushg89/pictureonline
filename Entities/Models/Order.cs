﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("order")]
    public class Order
    {
        [Key]
        public int OrderId  { get; set; }

        
        public decimal OrderMinimumWidth { get; set; }
    }
}
