USE [master]
GO
/****** Object:  Database [photoonline]    Script Date: 21-8-2019 10:13:17 ******/
CREATE DATABASE [photoonline]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'photoonline', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\photoonline.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'photoonline_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\photoonline_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [photoonline] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [photoonline].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [photoonline] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [photoonline] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [photoonline] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [photoonline] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [photoonline] SET ARITHABORT OFF 
GO
ALTER DATABASE [photoonline] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [photoonline] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [photoonline] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [photoonline] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [photoonline] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [photoonline] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [photoonline] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [photoonline] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [photoonline] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [photoonline] SET  DISABLE_BROKER 
GO
ALTER DATABASE [photoonline] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [photoonline] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [photoonline] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [photoonline] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [photoonline] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [photoonline] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [photoonline] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [photoonline] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [photoonline] SET  MULTI_USER 
GO
ALTER DATABASE [photoonline] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [photoonline] SET DB_CHAINING OFF 
GO
ALTER DATABASE [photoonline] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [photoonline] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [photoonline] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [photoonline] SET QUERY_STORE = OFF
GO
USE [photoonline]
GO
/****** Object:  Table [dbo].[order]    Script Date: 21-8-2019 10:13:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order](
	[orderid] [int] NOT NULL,
	[orderminimumwidth] [decimal](10, 2) NOT NULL,
 CONSTRAINT [PK_order] PRIMARY KEY CLUSTERED 
(
	[orderid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product]    Script Date: 21-8-2019 10:13:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[productid] [int] NOT NULL,
	[productname] [char](20) NOT NULL,
	[productwidth] [decimal](10, 2) NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[productid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[productorder]    Script Date: 21-8-2019 10:13:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productorder](
	[orderid] [int] NOT NULL,
	[productid] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[order] ([orderid], [orderminimumwidth]) VALUES (1, CAST(100.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[product] ([productid], [productname], [productwidth]) VALUES (1, N'photobook           ', CAST(19.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[product] ([productid], [productname], [productwidth]) VALUES (2, N'calender            ', CAST(10.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[product] ([productid], [productname], [productwidth]) VALUES (3, N'canvas              ', CAST(16.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[product] ([productid], [productname], [productwidth]) VALUES (4, N'cards               ', CAST(4.70 AS Decimal(10, 2)))
GO
INSERT [dbo].[product] ([productid], [productname], [productwidth]) VALUES (5, N'mug                 ', CAST(94.00 AS Decimal(10, 2)))
GO
SET IDENTITY_INSERT [dbo].[productorder] ON 
GO
INSERT [dbo].[productorder] ([orderid], [productid], [quantity], [id]) VALUES (1, 1, 2, 1)
GO
INSERT [dbo].[productorder] ([orderid], [productid], [quantity], [id]) VALUES (1, 2, 3, 2)
GO
SET IDENTITY_INSERT [dbo].[productorder] OFF
GO
ALTER TABLE [dbo].[productorder]  WITH CHECK ADD  CONSTRAINT [FK_productorder_order] FOREIGN KEY([productid])
REFERENCES [dbo].[product] ([productid])
GO
ALTER TABLE [dbo].[productorder] CHECK CONSTRAINT [FK_productorder_order]
GO
USE [master]
GO
ALTER DATABASE [photoonline] SET  READ_WRITE 
GO
