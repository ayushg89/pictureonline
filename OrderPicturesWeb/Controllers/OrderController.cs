﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.ExtendedModels;
using Microsoft.AspNetCore.Mvc;

namespace OrderPicturesWeb.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderController : ControllerBase
    {

        private IRepositoryWrapper _repository;

        public OrderController(IRepositoryWrapper repository)
        {

            _repository = repository;
        }

        [HttpGet("{id}")]
        public IActionResult GetOrderById(int id)
        {
            try
            {
                var order = _repository.Order.GetOrderById(id);

                if (order == null)
                {

                    return NotFound();
                }
                else
                {

                    return Ok(order);
                }
            }
            catch (Exception ex)
            {

                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public IActionResult CreateOrder(List<NewOrderRequestObj> obj)
        {

            try
            {
                var order = _repository.Order.CreatOrder(obj);

                if (order == 0)
                {

                    return NotFound();
                }
                else
                {

                    return Ok(order);
                }
            }
            catch (Exception ex)
            {

                return StatusCode(500, "Internal server error");
            }

        }
    }
}