﻿using Entities.ExtendedModels;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IOrderRepository:IRepositoryBase<Order>
    {
               
        OrderExtended GetOrderById(int orderId);

        decimal CreatOrder(List<NewOrderRequestObj> orderDetails);

        void AddOrder(Order order);

    }
}
