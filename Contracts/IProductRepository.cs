﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IProductRepository 
    {
        IEnumerable<Product> GetAllProduct();
        Product GetProductById(int productId);
    }
}
