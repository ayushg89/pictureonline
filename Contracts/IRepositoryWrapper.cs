﻿namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IProductRepository Product { get; }
        IProductOrderRepository ProductOrder { get; }

        IOrderRepository Order { get; }
        void Save();
    }
}